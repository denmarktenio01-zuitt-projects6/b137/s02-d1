package batch137.tenio.s02d1;

public class ArrayAndOperators {

    public static void main(String[] args) {
        // Declare an array of integers
        int[] intArray = new int[5];

        System.out.println("The element at index 0 is: " + intArray[0]);
        System.out.println("The element at index 0 is: " + intArray[1]);
        System.out.println("The element at index 0 is: " + intArray[2]);
        System.out.println("The element at index 0 is: " + intArray[3]);
        System.out.println("The element at index 0 is: " + intArray[4]);


        //mini aktibiti

        intArray[0] = 30;
        intArray[1] = 11;
        intArray[2] = 23;
        intArray[3] = 35;
        intArray[4] = 0;

        System.out.println("The element at index 0 is: " + intArray[0]);
        System.out.println("The element at index 0 is: " + intArray[1]);
        System.out.println("The element at index 0 is: " + intArray[2]);
        System.out.println("The element at index 0 is: " + intArray[3]);
        System.out.println("The element at index 0 is: " + intArray[4]);

        System.out.println();

        // declare an array of names
        String[] stringArray = {"Curry", "Curry Powder", "Totoy Mola"};

        System.out.println("#30: " + stringArray[0]);

        System.out.println();



    }
}
